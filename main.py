from toolkit import *

dna_size = int(input("Size of DNA sequence to generate: "))

dna_ = dna(dna_size)
complement_ = complement_dna(dna_)

print(f'\tGenerated DNA seq: {colored(dna_)}')
print(f'\tNumber of nucleotides: {count_nuc(dna_)}')
print(f'\tReverseComplement: {colored(reverse_complement(dna_))}')

print(f'\n\n\t{colored(dna_)}')
print(f'\t{"|"*dna_size}')
print(f'\t{colored(complement_)}')

