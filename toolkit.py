from structures import *


def validate_dna(sequence):
  for nuc in sequence.upper():
    if not nuc in nucleotides_dna:
      return False
  return True


def count_nuc(sequence):
  """Counts the number of nucleotides in sequence"""
  counters = {"A":0  ,"C":0, "G":0, "T":0, "U":0}
  for nuc in sequence.upper():    
    counters[ nuc ] += 1
  return counters

def complement_dna(seq):
  """Finds the complement for DNA sequence. Swapping adenine with thymine and guanine with cytosine"""
  
  result = ""
  for nuc in seq.upper():
    result += ( DNA_complement[nuc] )
  return result

###################
# TUT 2

def transcription(seq):
  """Replaces Thymine with Uracil"""
  return seq.replace("T","U")

def reverse_complement(seq):
  """Swapping adenine with thymine and guanine with cytosine. Reversing newly generated string"""
  return complement_dna(seq)[::-1]


class style:
  """Defines the colors for printing"""
  BLACK = '\033[30m'
  RED = '\033[31m'
  GREEN = '\033[32m'
  YELLOW = '\033[33m'
  BLUE = '\033[34m'
  MAGENTA = '\033[35m'
  CYAN = '\033[36m'
  WHITE = '\033[37m'
  UNDERLINE = '\033[4m'
  RESET = '\033[0m'

def colored(seq):
  result = style.RESET  
  color_code = {"A":style.GREEN , "C":style.BLUE, "G":style.YELLOW, "T":style.RED, "U":style.MAGENTA}
  for nuc in seq:
    result += ( color_code.get(nuc,style.RESET) + nuc)
  result += (style.RESET)
  return result


  
