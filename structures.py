import random

nucleotides_dna = ["A", "C", "G", "T"]
nucleotides_rna = ["A", "C", "G", "U"]

DNA_complement = {"A": "T", "C":"G", "G":"C", "T":"A"}


def dna(n):
  """Generates a DNA sequence of size n"""
  result = "" 
  for i in range(n):
    result += random.choice(nucleotides_dna)
  return result

def rna(n):
  """Generates a RNA sequence of size n"""
  result = ""
  for i in range(n):
    result += random.choice(nucleotides_rna)
  return result